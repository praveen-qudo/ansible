---
- name: Install aptitude using apt
  apt: name=aptitude state=latest update_cache=yes force_apt_get=yes

- name: Install required system packages
  apt: name={{ item }} state=latest update_cache=yes
  loop: [ 'apt-transport-https', 'ca-certificates', 'curl', 'software-properties-common', 'python3-pip', 'virtualenv', 'python3-setuptools', 'gnupg-agent']

- name: Add Docker GPG apt Key
  apt_key:
    url: https://download.docker.com/linux/ubuntu/gpg
    state: present

- name: Add Docker Repository
  apt_repository:
    repo: deb https://download.docker.com/linux/ubuntu bionic stable
    state: present

- name: Update apt and install docker-ce
  apt: update_cache=yes name=docker-ce state=latest

- name: Ensure docker is installed
  # https://docs.ansible.com/ansible/latest/modules/apt_module.html
  apt:
    name:
      - docker-ce
      - docker-ce-cli
      - containerd.io
    state: present
    update_cache: true

- name: Ensure Docker is started and enabled at boot
  # https://docs.ansible.com/ansible/latest/modules/service_module.html
  service:
    name: docker
    state: started
    enabled: true

- name: Ensure handlers are notified now to avoid firewall conflicts
  # https://docs.ansible.com/ansible/latest/modules/meta_module.html
  meta: flush_handlers

- name: "Ensure the user {{ ansible_user }} is part of the docker group"
  # https://docs.ansible.com/ansible/latest/modules/user_module.html
  user: 
    name: "{{ ansible_user }}"
    groups: docker
    append: yes

- name: Ensure docker python module and jsondiff are installed
  # https://docs.ansible.com/ansible/latest/modules/pip_module.html
  pip:
    name: 
      - docker
      # jsondiff and pyyaml are needed by the docker_stack module
      - jsondiff
      - pyyaml
  register: pip_install_docker
  ignore_errors: true

- name: Fetching pip
  # https://docs.ansible.com/ansible/latest/modules/get_url_module.html
  get_url:
    url: https://bootstrap.pypa.io/get-pip.py
    dest: "/home/{{ ansible_user }}/get-pip.py"
    mode: u=rwx,g=rwx,o=rx
  when: pip_install_docker is failed

- name: Installing pip
  # https://docs.ansible.com/ansible/latest/modules/command_module.html
  command: "python /home/{{ ansible_user }}/get-pip.py"
  when: pip_install_docker is failed

- name: Installing docker python module and jsondiff
  # https://docs.ansible.com/ansible/latest/modules/pip_module.html
  pip:
    name: 
      - docker
      # jsondiff and pyyaml are needed by the docker_stack module
      - jsondiff
      - pyyaml
  when: pip_install_docker is failed
